const sandwichCommentAnchor = document.querySelector('.jsChangeComment');
const originalCommentDiv = document.querySelector('.originalCommentHolder');
const originalCommentH1 = document.querySelector('.originalCommentH1');
const swappedCommentDiv = document.querySelector('.swappedCommentHolder');
const swappedComment = document.querySelector('.swappedCommentH1');
let count = 0;
let innerCount = 0;

sandwichCommentAnchor.addEventListener('click', (e) => {
  if (count === 0) {
    originalCommentDiv.setAttribute('style', 'display: none');
    createSwappedComment("..Chock-full of potential recruits");
    count++;
  } else if (count > 0) {
    swappedCommentDiv.setAttribute('style', 'display: none');
    createSwappedComment("Our job day is..");
  }

})

function createNode(parent, elementType, html) {
  const element = document.createElement(elementType);
  parent.append(element);
  element.innerHTML = html;
  return element;
}

function createSwappedComment(String) {
  if (count === 0 && innerCount ===0) {
    const swappedCommentH1 = createNode(swappedCommentDiv, 'div', `<h1 class="display-4">` + `${String}` + `</h1>`);
    swappedCommentH1.className = "swappedCommentH1";
  } else if (count > 0) {
    originalCommentDiv.innerHTML='';
    const originalCommentH1 = createNode(originalCommentDiv, 'div', `<h1 class="display-4">` + `${String}` + `</h1>`);
    originalCommentH1.className = "originalCommentH1";
    originalCommentDiv.setAttribute('style', 'display:visible');
    count = 0;
    innerCount++;
  } else if (count ===0 && innerCount > 0) {
    swappedCommentDiv.innerHTML = '';
    const swappedCommentH1 = createNode(swappedCommentDiv, 'div', `<h1 class="display-4">` + `${String}` + `</h1>`);
    swappedCommentH1.className = "swappedCommentH1";
    swappedCommentDiv.setAttribute('style', 'display:visible');
  }
}






// function clearHeading() {
//   sandwichOutput.innerHTML = '';
//}